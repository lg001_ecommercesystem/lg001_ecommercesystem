<?php

namespace Database\Seeders;

use Database\migrations;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        // Create products

        Product::truncate();

        $products = [

            //Electronics:

              ["name" => "iPhone", "category_id" =>"1" ,"price" =>"5.6"],
              ["name" => "Samsung Galaxy", "category_id" =>"1" ,"price" =>"5.6"],
              ["name" => "MacBook", "category_id" =>"1" ,"price" =>"5.6"],
              ["name" => "Dell XPS", "category_id" =>"1" ,"price" =>"5.6"],
              ["name" => "Canon EOS", "category_id" =>"1" ,"price" =>"5.6"],
              ["name" => "Sony Bravia", "category_id" =>"1" ,"price" =>"5.6"],
              ["name" => "Bose QuietComfort", "category_id" =>"1" ,"price" =>"5.6"],
              ["name" => "Sony WH-1000XM4", "category_id" =>"1" ,"price" =>"5.6"],
              ["name" => "Nikon D850", "category_id" =>"1" ,"price" =>"5.6"],
              ["name" => "LG OLED", "category_id" =>"1" ,"price" =>"5.6"],

            //Fashion and Apparel:


        ];
        foreach ($products as $key => $data) {
            Product::create($data);
        }




    }
}
