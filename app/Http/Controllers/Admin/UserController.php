<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public $route = 'admin/users';
    public $rootFolder = 'admin.users';

    public function index()
    {
        $records = User::get();
        return response()->json([
            'success' => true,
            'description' => 'Data retrieved successfully',
            'data' => $records
        ]);
    }

    public function create()
    {
        return view($this->rootFolder . ".create");
    }

    public function store(Request $request)
{
    $isEmailExists = User::where('email', $request->email)->exists();
    if ($isEmailExists) {
        return response()->json([
            'success' => false,
            'description' => 'Email already taken',
        ]);
    }

    $record = new User();
    $record->name = $request->name;
    $record->email = $request->email;
    $record->phone = $request->phone;
    //$record->status = 1;
    $record->password = Hash::make($request->password);
    $record->user_type = $request->user_type; // Administrator(1) or Guest(2);
    $record->save();

    return response()->json([
        'success' => true,
        'description' => 'User created successfully',
    ]);
}

    public function show(string $id)
    {
        $data['record'] = User::findOrFail($id);
        return view($this->rootFolder . ".show", $data);
    }

    public function edit(string $id)
    {
        $data['record'] = User::findOrFail($id);
        return view($this->rootFolder . ".edit", $data);
    }

    public function update(Request $request, string $id)
    {
        $record = User::findOrFail($id);
        $record->name = $request->name;
        $record->email = $request->email;
        $record->phone = $request->phone;
        if (isset($request->password)) {
            $record->password = Hash::make($request->password);
        }
        $record->save();

        return back();
    }

    public function destroy(string $id)
    {
        User::findOrFail($id)->delete();

        return back();

    }
}
